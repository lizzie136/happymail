//
//  main.m
//  Happy Mail
//
//  Created by Elizabeth Portilla Flores on 19/09/13.
//  Copyright (c) 2013 Elizabeth Portilla Flores. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HMAppDelegate class]));
    }
}
