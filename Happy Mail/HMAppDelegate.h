//
//  HMAppDelegate.h
//  Happy Mail
//
//  Created by Elizabeth Portilla Flores on 19/09/13.
//  Copyright (c) 2013 Elizabeth Portilla Flores. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
